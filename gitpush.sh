#!/bin/bash

echo reset
git reset

git config --global user.email "dev@realmethods.com"
git config --global user.name "Scrum Master"
 
echo init the repository
git init

echo add all files from root dir below, with ignore dirs and files in the .gitignore
git add .

echo 'commit all the files'
git commit -m "initial commit"

echo 'remove the repository'
git remote rm SpringMongoDB

echo 'add a remote pseudo for the SpringMongoDB repository'
git remote add SpringMongoDB https://complexmathguy:6969Cutlass!!@gitlab.com/complexmathguy/SpringMongoDB

echo 'delete tag'
git tag -d latest

git push --delete SpringMongoDB latest

echo 'add tag'
git tag latest

echo 'push tag'
git push SpringMongoDB latest

echo 'push the commits to the repository and append a new branch SpringMongoDB to SpringMongoDB'
git push SpringMongoDB master:SpringMongoDB
