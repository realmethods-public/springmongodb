/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * QuestionGroup business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of QuestionGroup related services in the case of a QuestionGroup business related service failing.</li>
 * <li>Exposes a simpler, uniform QuestionGroup interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill QuestionGroup business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class QuestionGroupBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public QuestionGroupBusinessDelegate() 
	{
	}

        
   /**
	* QuestionGroup Business Delegate Factory Method
	*
	* Returns a singleton instance of QuestionGroupBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	QuestionGroupBusinessDelegate
	*/
	public static QuestionGroupBusinessDelegate getQuestionGroupInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new QuestionGroupBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the QuestionGroup via an QuestionGroupPrimaryKey.
     * @param 	key
     * @return 	QuestionGroup
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public QuestionGroup getQuestionGroup( QuestionGroupPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "QuestionGroupBusinessDelegate:getQuestionGroup - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        QuestionGroup returnBO = null;
                
        QuestionGroupDAO dao = getQuestionGroupDAO();
        
        try
        {
            returnBO = dao.findQuestionGroup( key );
        }
        catch( Exception exc )
        {
            String errMsg = "QuestionGroupBusinessDelegate:getQuestionGroup( QuestionGroupPrimaryKey key ) - unable to locate QuestionGroup with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all QuestionGroups
     *
     * @return 	ArrayList<QuestionGroup> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<QuestionGroup> getAllQuestionGroup() 
    throws ProcessingException
    {
    	String msgPrefix				= "QuestionGroupBusinessDelegate:getAllQuestionGroup() - ";
        ArrayList<QuestionGroup> array	= null;
        
        QuestionGroupDAO dao = getQuestionGroupDAO();
    
        try
        {
            array = dao.findAllQuestionGroup();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	QuestionGroup
    * @return       QuestionGroup
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public QuestionGroup createQuestionGroup( QuestionGroup businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "QuestionGroupBusinessDelegate:createQuestionGroup - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        QuestionGroupDAO dao  = getQuestionGroupDAO();
        
        try
        {
            businessObject = dao.createQuestionGroup( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "QuestionGroupBusinessDelegate:createQuestionGroup() - Unable to create QuestionGroup" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		QuestionGroup
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public QuestionGroup saveQuestionGroup( QuestionGroup businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "QuestionGroupBusinessDelegate:saveQuestionGroup - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        QuestionGroupPrimaryKey key = businessObject.getQuestionGroupPrimaryKey();
                    
        if ( key != null )
        {
            QuestionGroupDAO dao = getQuestionGroupDAO();

            try
            {                    
                businessObject = (QuestionGroup)dao.saveQuestionGroup( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "QuestionGroupBusinessDelegate:saveQuestionGroup() - Unable to save QuestionGroup" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseQuestionGroupDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "QuestionGroupBusinessDelegate:saveQuestionGroup() - Unable to create QuestionGroup due to it having a null QuestionGroupPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	QuestionGroupPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( QuestionGroupPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "QuestionGroupBusinessDelegate:saveQuestionGroup - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        QuestionGroupDAO dao  = getQuestionGroupDAO();

        try
        {                    
            dao.deleteQuestionGroup( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete QuestionGroup using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the QuestionGroup specific DAO.
     *
     * @return      QuestionGroup DAO
     */
    public QuestionGroupDAO getQuestionGroupDAO()
    {
        return( new com.occulue.dao.QuestionGroupDAO() ); 
    }

    /**
     * Release the QuestionGroupDAO back to the FrameworkDAOFactory
     */
    public void releaseQuestionGroupDAO( com.occulue.dao.QuestionGroupDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static QuestionGroupBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(QuestionGroup.class.getName());
    
}



