/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * TheReference business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of TheReference related services in the case of a TheReference business related service failing.</li>
 * <li>Exposes a simpler, uniform TheReference interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill TheReference business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class TheReferenceBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public TheReferenceBusinessDelegate() 
	{
	}

        
   /**
	* TheReference Business Delegate Factory Method
	*
	* Returns a singleton instance of TheReferenceBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	TheReferenceBusinessDelegate
	*/
	public static TheReferenceBusinessDelegate getTheReferenceInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new TheReferenceBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the TheReference via an TheReferencePrimaryKey.
     * @param 	key
     * @return 	TheReference
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public TheReference getTheReference( TheReferencePrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "TheReferenceBusinessDelegate:getTheReference - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        TheReference returnBO = null;
                
        TheReferenceDAO dao = getTheReferenceDAO();
        
        try
        {
            returnBO = dao.findTheReference( key );
        }
        catch( Exception exc )
        {
            String errMsg = "TheReferenceBusinessDelegate:getTheReference( TheReferencePrimaryKey key ) - unable to locate TheReference with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all TheReferences
     *
     * @return 	ArrayList<TheReference> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<TheReference> getAllTheReference() 
    throws ProcessingException
    {
    	String msgPrefix				= "TheReferenceBusinessDelegate:getAllTheReference() - ";
        ArrayList<TheReference> array	= null;
        
        TheReferenceDAO dao = getTheReferenceDAO();
    
        try
        {
            array = dao.findAllTheReference();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	TheReference
    * @return       TheReference
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public TheReference createTheReference( TheReference businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "TheReferenceBusinessDelegate:createTheReference - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        TheReferenceDAO dao  = getTheReferenceDAO();
        
        try
        {
            businessObject = dao.createTheReference( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "TheReferenceBusinessDelegate:createTheReference() - Unable to create TheReference" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		TheReference
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public TheReference saveTheReference( TheReference businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "TheReferenceBusinessDelegate:saveTheReference - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        TheReferencePrimaryKey key = businessObject.getTheReferencePrimaryKey();
                    
        if ( key != null )
        {
            TheReferenceDAO dao = getTheReferenceDAO();

            try
            {                    
                businessObject = (TheReference)dao.saveTheReference( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "TheReferenceBusinessDelegate:saveTheReference() - Unable to save TheReference" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseTheReferenceDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "TheReferenceBusinessDelegate:saveTheReference() - Unable to create TheReference due to it having a null TheReferencePrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	TheReferencePrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( TheReferencePrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "TheReferenceBusinessDelegate:saveTheReference - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        TheReferenceDAO dao  = getTheReferenceDAO();

        try
        {                    
            dao.deleteTheReference( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete TheReference using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the TheReference specific DAO.
     *
     * @return      TheReference DAO
     */
    public TheReferenceDAO getTheReferenceDAO()
    {
        return( new com.occulue.dao.TheReferenceDAO() ); 
    }

    /**
     * Release the TheReferenceDAO back to the FrameworkDAOFactory
     */
    public void releaseTheReferenceDAO( com.occulue.dao.TheReferenceDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static TheReferenceBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(TheReference.class.getName());
    
}



