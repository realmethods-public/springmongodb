/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * TheResponse business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of TheResponse related services in the case of a TheResponse business related service failing.</li>
 * <li>Exposes a simpler, uniform TheResponse interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill TheResponse business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class TheResponseBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public TheResponseBusinessDelegate() 
	{
	}

        
   /**
	* TheResponse Business Delegate Factory Method
	*
	* Returns a singleton instance of TheResponseBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	TheResponseBusinessDelegate
	*/
	public static TheResponseBusinessDelegate getTheResponseInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new TheResponseBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the TheResponse via an TheResponsePrimaryKey.
     * @param 	key
     * @return 	TheResponse
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public TheResponse getTheResponse( TheResponsePrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "TheResponseBusinessDelegate:getTheResponse - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        TheResponse returnBO = null;
                
        TheResponseDAO dao = getTheResponseDAO();
        
        try
        {
            returnBO = dao.findTheResponse( key );
        }
        catch( Exception exc )
        {
            String errMsg = "TheResponseBusinessDelegate:getTheResponse( TheResponsePrimaryKey key ) - unable to locate TheResponse with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all TheResponses
     *
     * @return 	ArrayList<TheResponse> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<TheResponse> getAllTheResponse() 
    throws ProcessingException
    {
    	String msgPrefix				= "TheResponseBusinessDelegate:getAllTheResponse() - ";
        ArrayList<TheResponse> array	= null;
        
        TheResponseDAO dao = getTheResponseDAO();
    
        try
        {
            array = dao.findAllTheResponse();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	TheResponse
    * @return       TheResponse
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public TheResponse createTheResponse( TheResponse businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "TheResponseBusinessDelegate:createTheResponse - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        TheResponseDAO dao  = getTheResponseDAO();
        
        try
        {
            businessObject = dao.createTheResponse( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "TheResponseBusinessDelegate:createTheResponse() - Unable to create TheResponse" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		TheResponse
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public TheResponse saveTheResponse( TheResponse businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "TheResponseBusinessDelegate:saveTheResponse - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        TheResponsePrimaryKey key = businessObject.getTheResponsePrimaryKey();
                    
        if ( key != null )
        {
            TheResponseDAO dao = getTheResponseDAO();

            try
            {                    
                businessObject = (TheResponse)dao.saveTheResponse( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "TheResponseBusinessDelegate:saveTheResponse() - Unable to save TheResponse" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseTheResponseDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "TheResponseBusinessDelegate:saveTheResponse() - Unable to create TheResponse due to it having a null TheResponsePrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	TheResponsePrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( TheResponsePrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "TheResponseBusinessDelegate:saveTheResponse - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        TheResponseDAO dao  = getTheResponseDAO();

        try
        {                    
            dao.deleteTheResponse( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete TheResponse using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the TheResponse specific DAO.
     *
     * @return      TheResponse DAO
     */
    public TheResponseDAO getTheResponseDAO()
    {
        return( new com.occulue.dao.TheResponseDAO() ); 
    }

    /**
     * Release the TheResponseDAO back to the FrameworkDAOFactory
     */
    public void releaseTheResponseDAO( com.occulue.dao.TheResponseDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static TheResponseBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(TheResponse.class.getName());
    
}



