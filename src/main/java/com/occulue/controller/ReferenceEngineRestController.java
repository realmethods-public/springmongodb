/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity ReferenceEngine.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/ReferenceEngine")
public class ReferenceEngineRestController extends BaseSpringRestController
{

    /**
     * Handles saving a ReferenceEngine BO.  if not key provided, calls create, otherwise calls save
     * @param		ReferenceEngine referenceEngine
     * @return		ReferenceEngine
     */
	@RequestMapping("/save")
    public ReferenceEngine save( @RequestBody ReferenceEngine referenceEngine )
    {
    	// assign locally
    	this.referenceEngine = referenceEngine;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a ReferenceEngine BO
     * @param		Long referenceEngineId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="referenceEngine.referenceEngineId", required=false) Long referenceEngineId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ReferenceEngineBusinessDelegate delegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ReferenceEnginePrimaryKey( referenceEngineId ) );
        		LOGGER.info( "ReferenceEngineController:delete() - successfully deleted ReferenceEngine with key " + referenceEngine.getReferenceEnginePrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferenceEnginePrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ReferenceEngineController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceEngineController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a ReferenceEngine BO
     * @param		Long referenceEngineId
     * @return		ReferenceEngine
     */    
    @RequestMapping("/load")
    public ReferenceEngine load( @RequestParam(value="referenceEngine.referenceEngineId", required=true) Long referenceEngineId )
    {    	
        ReferenceEnginePrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****ReferenceEngine.load pk is " + referenceEngineId );
        	if ( referenceEngineId != null )
        	{
        		pk = new ReferenceEnginePrimaryKey( referenceEngineId );
        		
        		// load the ReferenceEngine
	            this.referenceEngine = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().getReferenceEngine( pk );
	            
	            LOGGER.info( "ReferenceEngineController:load() - successfully loaded - " + this.referenceEngine.toString() );             
			}
			else
			{
	            LOGGER.info( "ReferenceEngineController:load() - unable to locate the primary key as an attribute or a selection for - " + referenceEngine.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ReferenceEngineController:load() - failed to load ReferenceEngine using Id " + referenceEngineId + ", " + exc.getMessage() );
            return null;
        }

        return referenceEngine;

    }

    /**
     * Handles loading all ReferenceEngine business objects
     * @return		List<ReferenceEngine>
     */
    @RequestMapping("/loadAll")
    public List<ReferenceEngine> loadAll()
    {                
        List<ReferenceEngine> referenceEngineList = null;
        
    	try
        {                        
            // load the ReferenceEngine
            referenceEngineList = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().getAllReferenceEngine();
            
            if ( referenceEngineList != null )
                LOGGER.info(  "ReferenceEngineController:loadAllReferenceEngine() - successfully loaded all ReferenceEngines" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ReferenceEngineController:loadAll() - failed to load all ReferenceEngines - " + exc.getMessage() );
        	return null;
            
        }

        return referenceEngineList;
                            
    }


// findAllBy methods


    /**
     * save MainQuestionGroup on ReferenceEngine
     * @param		Long	referenceEngineId
     * @param		Long	childId
     * @param		ReferenceEngine referenceEngine
     * @return		ReferenceEngine
     */     
	@RequestMapping("/saveMainQuestionGroup")
	public ReferenceEngine saveMainQuestionGroup( @RequestParam(value="referenceEngine.referenceEngineId", required=true) Long referenceEngineId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody ReferenceEngine referenceEngine )
	{
		if ( load( referenceEngineId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			ReferenceEngineBusinessDelegate parentDelegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();			
			QuestionGroup child 							= null;

			try
			{
				child = childDelegate.getQuestionGroup( new QuestionGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ReferenceEngineController:saveMainQuestionGroup() failed to get QuestionGroup using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			referenceEngine.setMainQuestionGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceEngine( referenceEngine );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceEngineController:saveMainQuestionGroup() failed saving parent ReferenceEngine - " + exc.getMessage() );
			}
		}
		
		return referenceEngine;
	}

    /**
     * delete MainQuestionGroup on ReferenceEngine
     * @param		Long referenceEngineId
     * @return		ReferenceEngine
     */     
	@RequestMapping("/deleteMainQuestionGroup")
	public ReferenceEngine deleteMainQuestionGroup( @RequestParam(value="referenceEngine.referenceEngineId", required=true) Long referenceEngineId )
	
	{
		if ( load( referenceEngineId ) == null )
			return( null );

		if ( referenceEngine.getMainQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = referenceEngine.getMainQuestionGroup().getQuestionGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceEngine.setMainQuestionGroup( null );
			try
			{
				ReferenceEngineBusinessDelegate parentDelegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();

				// save it
				referenceEngine = parentDelegate.saveReferenceEngine( referenceEngine );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceEngineController:deleteMainQuestionGroup() failed to save ReferenceEngine - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferenceEngineController:deleteMainQuestionGroup() failed  - " + exc.getMessage() );
			}
		}
		
		return referenceEngine;
	}
	


    /**
     * Handles creating a ReferenceEngine BO
     * @return		ReferenceEngine
     */
    protected ReferenceEngine create()
    {
        try
        {       
			this.referenceEngine = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().createReferenceEngine( referenceEngine );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceEngineController:create() - exception ReferenceEngine - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceEngine;
    }

    /**
     * Handles updating a ReferenceEngine BO
     * @return		ReferenceEngine
     */    
    protected ReferenceEngine update()
    {
    	// store provided data
        ReferenceEngine tmp = referenceEngine;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	referenceEngine.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferenceEngineBusiness Delegate            
			ReferenceEngineBusinessDelegate delegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
            this.referenceEngine = delegate.saveReferenceEngine( referenceEngine );
            
            if ( this.referenceEngine != null )
                LOGGER.info( "ReferenceEngineController:update() - successfully updated ReferenceEngine - " + referenceEngine.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferenceEngineController:update() - successfully update ReferenceEngine - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referenceEngine;
        
    }


    /**
     * Returns true if the referenceEngine is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referenceEngine != null && referenceEngine.getReferenceEnginePrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected ReferenceEngine load()
    {
    	return( load( new Long( referenceEngine.getReferenceEnginePrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected ReferenceEngine referenceEngine = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceEngine.class.getName());
    
}


