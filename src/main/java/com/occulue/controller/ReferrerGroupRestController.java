/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity ReferrerGroup.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/ReferrerGroup")
public class ReferrerGroupRestController extends BaseSpringRestController
{

    /**
     * Handles saving a ReferrerGroup BO.  if not key provided, calls create, otherwise calls save
     * @param		ReferrerGroup referrerGroup
     * @return		ReferrerGroup
     */
	@RequestMapping("/save")
    public ReferrerGroup save( @RequestBody ReferrerGroup referrerGroup )
    {
    	// assign locally
    	this.referrerGroup = referrerGroup;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a ReferrerGroup BO
     * @param		Long referrerGroupId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="referrerGroup.referrerGroupId", required=false) Long referrerGroupId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ReferrerGroupBusinessDelegate delegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ReferrerGroupPrimaryKey( referrerGroupId ) );
        		LOGGER.info( "ReferrerGroupController:delete() - successfully deleted ReferrerGroup with key " + referrerGroup.getReferrerGroupPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferrerGroupPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ReferrerGroupController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerGroupController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a ReferrerGroup BO
     * @param		Long referrerGroupId
     * @return		ReferrerGroup
     */    
    @RequestMapping("/load")
    public ReferrerGroup load( @RequestParam(value="referrerGroup.referrerGroupId", required=true) Long referrerGroupId )
    {    	
        ReferrerGroupPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****ReferrerGroup.load pk is " + referrerGroupId );
        	if ( referrerGroupId != null )
        	{
        		pk = new ReferrerGroupPrimaryKey( referrerGroupId );
        		
        		// load the ReferrerGroup
	            this.referrerGroup = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getReferrerGroup( pk );
	            
	            LOGGER.info( "ReferrerGroupController:load() - successfully loaded - " + this.referrerGroup.toString() );             
			}
			else
			{
	            LOGGER.info( "ReferrerGroupController:load() - unable to locate the primary key as an attribute or a selection for - " + referrerGroup.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ReferrerGroupController:load() - failed to load ReferrerGroup using Id " + referrerGroupId + ", " + exc.getMessage() );
            return null;
        }

        return referrerGroup;

    }

    /**
     * Handles loading all ReferrerGroup business objects
     * @return		List<ReferrerGroup>
     */
    @RequestMapping("/loadAll")
    public List<ReferrerGroup> loadAll()
    {                
        List<ReferrerGroup> referrerGroupList = null;
        
    	try
        {                        
            // load the ReferrerGroup
            referrerGroupList = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getAllReferrerGroup();
            
            if ( referrerGroupList != null )
                LOGGER.info(  "ReferrerGroupController:loadAllReferrerGroup() - successfully loaded all ReferrerGroups" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ReferrerGroupController:loadAll() - failed to load all ReferrerGroups - " + exc.getMessage() );
        	return null;
            
        }

        return referrerGroupList;
                            
    }


// findAllBy methods



    /**
     * save References on ReferrerGroup
     * @param		Long referrerGroupId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		ReferrerGroup
     */     
	@RequestMapping("/saveReferences")
	public ReferrerGroup saveReferences( @RequestParam(value="referrerGroup.referrerGroupId", required=false) Long referrerGroupId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( referrerGroupId ) == null )
			return( null );
		
		TheReferencePrimaryKey pk 					= null;
		TheReference child							= null;
		TheReferenceBusinessDelegate childDelegate 	= TheReferenceBusinessDelegate.getTheReferenceInstance();
		ReferrerGroupBusinessDelegate parentDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new TheReferencePrimaryKey( childId );
			
			try
			{
				// find the TheReference
				child = childDelegate.getTheReference( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferrerGroupController:saveReferences() failed get child TheReference using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the References 
			referrerGroup.getReferences().add( child );				
		}
		else
		{
			// clear out the References but 
			referrerGroup.getReferences().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new TheReferencePrimaryKey( id );
					try
					{
						// find the TheReference
						child = childDelegate.getTheReference( pk );
						// add it to the References List
						referrerGroup.getReferences().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "ReferrerGroupController:saveReferences() failed get child TheReference using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the ReferrerGroup
			parentDelegate.saveReferrerGroup( referrerGroup );
		}
		catch( Exception exc )
		{
			LOGGER.info( "ReferrerGroupController:saveReferences() failed saving parent ReferrerGroup - " + exc.getMessage() );
		}

		return referrerGroup;
	}

    /**
     * delete References on ReferrerGroup
     * @param		Long referrerGroupId
     * @param		Long[] childIds
     * @return		ReferrerGroup
     */     	
	@RequestMapping("/deleteReferences")
	public ReferrerGroup deleteReferences( @RequestParam(value="referrerGroup.referrerGroupId", required=true) Long referrerGroupId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( referrerGroupId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			TheReferencePrimaryKey pk 					= null;
			TheReferenceBusinessDelegate childDelegate 	= TheReferenceBusinessDelegate.getTheReferenceInstance();
			ReferrerGroupBusinessDelegate parentDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			Set<TheReference> children					= referrerGroup.getReferences();
			TheReference child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new TheReferencePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getTheReference( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "ReferrerGroupController:deleteReferences() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of TheReference back to the referrerGroup
			referrerGroup.setReferences( children );			
			// save it 
			try
			{
				referrerGroup = parentDelegate.saveReferrerGroup( referrerGroup );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "ReferrerGroupController:deleteReferences() failed to save the ReferrerGroup - " + exc.getMessage() );
			}
		}
		
		return referrerGroup;
	}


    /**
     * Handles creating a ReferrerGroup BO
     * @return		ReferrerGroup
     */
    protected ReferrerGroup create()
    {
        try
        {       
			this.referrerGroup = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().createReferrerGroup( referrerGroup );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerGroupController:create() - exception ReferrerGroup - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referrerGroup;
    }

    /**
     * Handles updating a ReferrerGroup BO
     * @return		ReferrerGroup
     */    
    protected ReferrerGroup update()
    {
    	// store provided data
        ReferrerGroup tmp = referrerGroup;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	referrerGroup.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferrerGroupBusiness Delegate            
			ReferrerGroupBusinessDelegate delegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
            this.referrerGroup = delegate.saveReferrerGroup( referrerGroup );
            
            if ( this.referrerGroup != null )
                LOGGER.info( "ReferrerGroupController:update() - successfully updated ReferrerGroup - " + referrerGroup.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerGroupController:update() - successfully update ReferrerGroup - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referrerGroup;
        
    }


    /**
     * Returns true if the referrerGroup is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referrerGroup != null && referrerGroup.getReferrerGroupPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected ReferrerGroup load()
    {
    	return( load( new Long( referrerGroup.getReferrerGroupPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected ReferrerGroup referrerGroup = null;
    private static final Logger LOGGER = Logger.getLogger(ReferrerGroup.class.getName());
    
}


